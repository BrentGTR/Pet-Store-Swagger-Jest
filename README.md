# SpriteCloud Automation Assignment

This project is using Jest(https://jestjs.io/) and SuperTest(https://www.npmjs.com/package/supertest).

## Quick Start & Documentation

## Setting up your environment
### Install Node.js and npm
Note: to download the latest version of npm, on the command line, run the following command:
npm install -g npm

### Install Jest, Jest-stare and Jest-serial-runner on the command line using the following command:
npm install jest jest-stare jest-serial-runner --save-dev

### Install SuperTest on the command line using the following command:
npm install supertest --save-dev

## How to run the tests locally
Prerequisite: Run the command npm install in the terminal
Using the command line: npm test

## Jest-stare writes output reports to ./jest-stare/index.html
## Gitlab reports are served at: https://brentgtr.gitlab.io/Pet-Store-Swagger-Jest/

## How to run the test in a CI/CD pipeline
Navigate to the project on Gitlab.com (https://gitlab.com/BrentGTR/Pet-Store-Swagger-Jest) and click on CI/CD in the sidebar. Then click on "Run Pipeline".


## Has a link to the results in Calliope.pro
https://app.calliope.pro/reports/110178/public/521a0ea7-450d-4510-9466-1cf3af78d537


## What you used to select the scenarios, what was your approach?
The approach taken in selecting the functionality to cover with the automated tests was based on an MVP of the pet 
store application. 
Hence, features such as create were selected as they would be the first action performed by a user when setting up the 
store. A login scenario was also selected as this feature is the first touch-point of the application for users. 


## Why are they the most important
Please see above.


## What could be the next steps to your project
Further steps are to complete the testing of the endpoints of the project as each relate to functionality of the 
application. Pact (https://docs.pact.io/) will be used in addition with Jest in order to perform contract testing for 
the API.

